FROM debian:bullseye

ARG user

RUN apt-get update \
    && apt-get install -y \
        ca-certificates \
    && rm -rf /var/lib/apt/lists/*

RUN addgroup --gid 1000 app \
    && adduser --uid 1000 --ingroup app --home /home/$user --shell /bin/sh --disabled-password --gecos "" app \
    && mkdir /app \
    && chown app:app /app

WORKDIR /app
